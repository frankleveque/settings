(setq gc-cons-threshold most-positive-fixnum)

;; enable lines
(global-linum-mode t)

(add-hook 'prog-mode-hook 'linum-mode)

;;dont wrap
(set-default 'truncate-lines t)

;; Enable backup files
(setq make-backup-files nil)

;; auto reload files
(global-auto-revert-mode t)

;;set min window sizes
(setq window-min-height 0)
(setq window-min-width 0)

;; Tabs to spaces
(setq-default indent-tabs-mode nil)

;; disable auto-save and auto-backup and lock files
(setq auto-save-default nil)
(setq make-backup-files nil)
(setq create-lockfiles nil)

;; remove fringe
(set-fringe-mode '(0 . 0))

;; row/column in footer
(setq column-number-mode t)

;; mouse wheel
(setq mouse-wheel-scroll-amount '(3 ((shift) . 3))) ;; 3 lines at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq scroll-conservatively 10000)

;; disable emacs startup screen
(setq inhibit-startup-screen t)

;; set scratch contents
(setq initial-scratch-message "")

;; remove toolbars
(tool-bar-mode -1)
(toggle-scroll-bar -1)
(scroll-bar-mode -1)
;(menu-bar-mode -1) 

;; disable bell
(setq ring-bell-function 'ignore)

;; case insensitive
(setq completion-ignore-case t)

;; utf8 
(prefer-coding-system 'utf-8)

;; remove org postamble
(setq org-html-postamble nil) 

;; add timestamp on closed
(setq org-log-done 'note)

;; remove org validation link
(setq org-html-validation-link nil)

;; org mode bindings
(global-set-key (kbd "<f5>" ) 'org-store-link)
(global-set-key (kbd "<f6>" ) 'org-agenda)
(global-set-key (kbd "<f7>" ) 'org-capture)
(global-set-key (kbd "<f8>" ) 'org-switchb)

;; minibuffer msg
(defun display-startup-echo-area-message ()
  (message (concat "Startup in: " (emacs-init-time))))

;; fix splits across multiple windows
(setq gdb-many-windows t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Package installs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)

; list the repositories containing them
(setq package-archives
      '(("elpa" . "http://tromey.com/elpa/")
        ("melpa" . "https://melpa.org/packages/")
        ("gnu" . "http://elpa.gnu.org/packages/")))
        ;("marmalade" . "https://marmalade-repo.org/packages/")))

; list the packages you want
(setq package-list '(
async
auto-complete 
cmake-mode 
company
counsel
counsel 
dockerfile-mode
evil 
flx 
gitattributes-mode 
gitconfig-mode 
gitignore-mode 
go-mode 
guess-offset
ivy 
json-mode 
jvm-mode 
lua-mode 
markdown-mode 
meson-mode 
molokai-theme 
org
python-mode 
slime 
smex
))

; activate all the packages (in particular autoloads)
(package-initialize)

; fetch the list of packages available 
(unless package-archive-contents
  (package-refresh-contents))

; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; lisp/slime
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set your lisp system and, optionally, some contribs
;(setq inferior-lisp-program "<path>")
;(setq slime-contribs '(slime-fancy))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; font 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 
    'default-frame-alist
	     '(font . "DejaVu Sans Mono-11:antialias=natural:weight=bold"))

(set-frame-font 
    "DejaVu Sans Mono-11:antialias=natural:weight=bold")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(load-theme 'molokai t)

(require 'evil)
(evil-mode 1)


